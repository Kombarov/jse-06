package ru.kombarov.tm.service;

import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.repository.UserRepository;

import java.util.List;

public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        else return userRepository.findOne(id);
    }

    public void persist(User user) throws Exception {
        if (user == null) throw new Exception();
        else userRepository.persist(user);
    }

    public void merge(User user) throws Exception {
        if (user == null) throw new Exception();
        else userRepository.merge(user);
    }

    public void remove(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        else userRepository.remove(id);
    }

    public void removeAll() {
        userRepository.removeAll();
    }

    public String getUserIdByName(String name) {
        if (name == null || name.isEmpty()) return null;
        else {
            List<User> list = userRepository.findAll();
            String id = "";
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getLogin().equals(name)) id = list.get(i).getId();
            }
            return id;
        }
    }
}
