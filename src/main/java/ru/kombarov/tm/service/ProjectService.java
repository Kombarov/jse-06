package ru.kombarov.tm.service;

import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        else return projectRepository.findOne(id);
    }

    public void persist(Project project) throws Exception {
        if (project == null) throw new Exception();
        else projectRepository.persist(project);
    }

    public void merge(Project project) throws Exception {
        if (project == null) throw new Exception();
        else projectRepository.merge(project);
    }

    public void remove(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        else projectRepository.remove(id);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public String getProjectIdByName(String name) {
        if (name == null || name.isEmpty()) return null;
        else {
            List<Project> list = projectRepository.findAll();
            String id = "";
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(name)) id = list.get(i).getId();
            }
            return id;
        }
    }

    public List<Project> getProjectsByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        else {
            List<Project> list = projectRepository.findAll();
            List<Project> sortedList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getUserId().equals(userId)) sortedList.add(list.get(i));
            }
            return sortedList;
        }
    }

    public List<Project> findAll(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findAll(userId);
    }

    public Project findOne(String userId, String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOne(userId, id);
    }

    public void removeAll(String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        projectRepository.removeAll(userId);
    }
}
