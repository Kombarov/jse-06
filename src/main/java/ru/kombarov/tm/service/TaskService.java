package ru.kombarov.tm.service;

import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        else return taskRepository.findOne(id);
    }

    public void persist(Task task) throws Exception {
        if (task == null) throw new Exception();
        else taskRepository.persist(task);
    }

    public void merge(Task task) throws Exception {
        if (task == null) throw new Exception();
        else taskRepository.merge(task);
    }

    public void remove(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        else taskRepository.remove(id);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public String getTaskIdByName(String name) {
        if (name == null || name.isEmpty()) return null;
        else {
            List<Task> list = taskRepository.findAll();
            String id = "";
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(name)) id = list.get(i).getId();
            }
            return id;
        }
    }

    public List<Task> getTasksByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        else {
            List<Task> list = taskRepository.findAll();
            List<Task> sortedList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getProjectId().equals(projectId)) sortedList.add(list.get(i));
            }
            return sortedList;
        }
    }

    public List<Task> getTasksByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        else {
            List<Task> list = taskRepository.findAll();
            List<Task> sortedList = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getUserId().equals(userId)) sortedList.add(list.get(i));
            }
            return sortedList;
        }
    }

    public List<Task> findAll(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findAll();
    }

    public Task findOne(String userId, String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(userId, id);
    }

    public void removeAll(String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        taskRepository.removeAll(userId);
    }
}
