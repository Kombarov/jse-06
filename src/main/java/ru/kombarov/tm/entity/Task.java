package ru.kombarov.tm.entity;

import java.util.Date;

import static ru.kombarov.tm.util.DateUtil.parseDateToString;

public class Task extends AbstractEntity {

    private String name;

    private String projectId;

    private String userId;

    private String description;

    private Date dateStart;

    private Date dateFinish;

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateStart() {
        return parseDateToString(dateStart);
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return parseDateToString(dateFinish);
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
