package ru.kombarov.tm.repository;

import ru.kombarov.tm.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {

    private Map<String, Project> projectMap = new HashMap<>();

    public List<Project> findAll() {
        List<Project> list = new ArrayList<Project>(projectMap.values());
        return list;
    }

    public Project findOne(String id) {
        return projectMap.get(id);
    }

    public void persist(Project project) {
        projectMap.put(project.getId(), project);
    }

    public void merge(Project project) {
        if (projectMap.containsKey(project.getId())) {
            projectMap.remove(project.getId());
            projectMap.put(project.getId(), project);
        }
        else {
            projectMap.put(project.getId(), project);
        }
    }

    public void remove(String id) {
        projectMap.remove(id);
    }

    public void removeAll() {
        projectMap.clear();
    }

    public List<Project> findAll(String userId) {
        List<Project> list = new ArrayList<Project>();
        for (Project project : findAll()) {
            if (project.getUserId().equals(userId)) list.add(project);
        }
        return list;
    }

    public Project findOne(String userId, String id) {
        for (Project project : findAll(userId)) {
            if (project.equals(id)) return project;
        }
        return null;
    }

    public void removeAll(String userId) {
        for (Project project : findAll()) {
            if (project.getUserId().equals(userId)) remove(project.getId());
        }
    }
}