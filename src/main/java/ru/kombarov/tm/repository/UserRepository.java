package ru.kombarov.tm.repository;

import ru.kombarov.tm.entity.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRepository {

    private Map<String, User> userMap = new HashMap<>();

    public List<User> findAll() {
        List<User> list = new ArrayList<User>(userMap.values());
        return list;
    }

    public User findOne(String id) {
        return userMap.get(id);
    }

    public void persist(User user) {
        userMap.put(user.getId(), user);
    }

    public void merge(User user) {
        if (userMap.containsKey(user.getId())) {
            userMap.remove(user.getId());
            userMap.put(user.getId(), user);
        }
        else {
            userMap.put(user.getId(), user);
        }
    }

    public void remove(String id) {
        userMap.remove(id);
    }

    public void removeAll() {
        userMap.clear();
    }
}
