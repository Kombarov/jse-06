package ru.kombarov.tm.enumerated;

public enum Role {
    ADMINISTRATOR("Administrator"),
    USER("User");

    private String roleName;

    Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }
}
