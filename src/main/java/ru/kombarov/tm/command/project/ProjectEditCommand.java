package ru.kombarov.tm.command.project;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Project;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;
import static ru.kombarov.tm.util.EntityUtil.printProjects;

public class ProjectEditCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "project-edit";
    }

    @Override
    public String description() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[PROJECT EDIT]");
            printProjects(bootstrap.getProjectService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("ENTER PROJECT NAME FOR EDIT");
            String nameAnotherProject = input.readLine();
            Project anotherProject = new Project(nameAnotherProject);
            System.out.println("ENTER PROJECT DESCRIPTION");
            anotherProject.setDescription(input.readLine());
            System.out.println("ENTER START DATE");
            anotherProject.setDateStart(parseStringToDate(input.readLine()));
            System.out.println("ENTER FINISH DATE");
            anotherProject.setDateFinish(parseStringToDate(input.readLine()));
            anotherProject.setId(bootstrap.getProjectService().getProjectIdByName(nameAnotherProject));
            anotherProject.setUserId(bootstrap.getUserCurrent().getId());
            bootstrap.getProjectService().merge(anotherProject);
            System.out.println("[OK]");
        }
    }
}
