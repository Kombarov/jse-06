package ru.kombarov.tm.command.project;

import ru.kombarov.tm.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printProject;
import static ru.kombarov.tm.util.EntityUtil.printProjects;

public class ProjectSelectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "project-select";
    }

    @Override
    public String description() {
        return "Select the project.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[PROJECT SELECT]");
            printProjects(bootstrap.getProjectService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("ENTER PROJECT NAME");
            printProject(bootstrap.getProjectService().findOne(bootstrap.getProjectService().getProjectIdByName(input.readLine())));
            System.out.println("[OK]");
        }
    }
}
