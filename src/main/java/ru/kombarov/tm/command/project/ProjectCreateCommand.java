package ru.kombarov.tm.command.project;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Project;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;

public class ProjectCreateCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[PROJECT CREATE]");
            System.out.println("ENTER PROJECT NAME");
            Project project = new Project(input.readLine());
            System.out.println("ENTER PROJECT DESCRIPTION");
            project.setDescription(input.readLine());
            System.out.println("ENTER START DATE");
            project.setDateStart(parseStringToDate(input.readLine()));
            System.out.println("ENTER FINISH DATE");
            project.setDateFinish(parseStringToDate(input.readLine()));
            project.setUserId(bootstrap.getUserCurrent().getId());
            bootstrap.getProjectService().persist(project);
            System.out.println("[OK]");
        }
    }
}