package ru.kombarov.tm.command.project;

import ru.kombarov.tm.command.AbstractCommand;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[PROJECT CLEAR]");
            bootstrap.getProjectService().removeAll(bootstrap.getUserCurrent().getId());
            System.out.println("[OK]");
        }
    }
}
