package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskRemoveCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK REMOVE]");
            printTasks(bootstrap.getTaskService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("ENTER TASK NAME FOR REMOVE");
            bootstrap.getTaskService().remove(bootstrap.getTaskService().getTaskIdByName(input.readLine()));
            System.out.println("[OK]");
        }
    }
}
