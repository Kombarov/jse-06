package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printTask;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskSelectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-select";
    }

    @Override
    public String description() {
        return "Select the task.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK SELECT]");
            printTasks(bootstrap.getTaskService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("ENTER TASK NAME");
            printTask(bootstrap.getTaskService().findOne(bootstrap.getTaskService().getTaskIdByName(input.readLine())));
            System.out.println("[OK]");
        }
    }
}
