package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskUnattachCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-unattach";
    }

    @Override
    public String description() {
        return "Unattach task from the project.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK UNATTACH]");
            printTasks(bootstrap.getTaskService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("ENTER TASK NAME");
            String taskName = input.readLine();
            Task task = bootstrap.getTaskService().findOne(bootstrap.getTaskService().getTaskIdByName(taskName));
            task.setProjectId("");
            System.out.println("[OK]");
        }
    }
}
