package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK CLEAR]");
            bootstrap.getTaskService().removeAll(bootstrap.getUserCurrent().getId());
            System.out.println("[OK]");
        }
    }
}
