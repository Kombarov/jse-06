package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskEditCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-edit";
    }

    @Override
    public String description() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK EDIT]");
            printTasks(bootstrap.getTaskService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("ENTER TASK NAME FOR EDIT");
            String nameAnotherTask = input.readLine();
            Task anotherTask = new Task(nameAnotherTask);
            System.out.println("ENTER TASK DESCRIPTION");
            anotherTask.setDescription(input.readLine());
            System.out.println("ENTER START DATE");
            anotherTask.setDateStart(parseStringToDate(input.readLine()));
            System.out.println("ENTER FINISH DATE");
            anotherTask.setDateFinish(parseStringToDate(input.readLine()));
            anotherTask.setId(bootstrap.getTaskService().getTaskIdByName(nameAnotherTask));
            anotherTask.setUserId(bootstrap.getUserCurrent().getId());
            bootstrap.getTaskService().merge(anotherTask);
            System.out.println("[OK]");
        }
    }
}
