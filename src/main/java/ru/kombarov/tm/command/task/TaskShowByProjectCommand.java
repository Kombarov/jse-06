package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printProjects;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskShowByProjectCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "task-show by project";
    }

    @Override
    public String description() {
        return "Show tasks by project.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK SHOW BY PROJECT]");
            printProjects(bootstrap.getProjectService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("ENTER PROJECT NAME");
            printTasks(bootstrap.getTaskService().getTasksByProjectId(bootstrap.getProjectService().getProjectIdByName(input.readLine())));
            System.out.println("[OK]");
        }
    }
}
