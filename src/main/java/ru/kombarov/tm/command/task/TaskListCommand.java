package ru.kombarov.tm.command.task;

import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public class TaskListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[TASK LIST]");
            printTasks(bootstrap.getTaskService().findAll(bootstrap.getUserCurrent().getId()));
            System.out.println("[OK]");
        }
    }
}
