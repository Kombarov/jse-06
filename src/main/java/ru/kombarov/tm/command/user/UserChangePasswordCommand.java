package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.PasswordUtil.generateHash;

public class UserChangePasswordCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "user-change password";
    }

    @Override
    public String description() {
        return "Change user password.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[USER CHANGE PASSWORD]");
            System.out.println("ENTER NEW PASSWORD");
            String password = input.readLine();
            User user = new User();
            user = bootstrap.getUserCurrent();
            user.setPassword(generateHash(password));
            bootstrap.setUserCurrent(user);
            bootstrap.getUserService().merge(user);
            System.out.println("[OK]");
        }
    }
}
