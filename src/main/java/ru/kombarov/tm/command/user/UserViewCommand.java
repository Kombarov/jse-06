package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.*;

public class UserViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-view";
    }

    @Override
    public String description() {
        return "Show user info.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[USER VIEW]");
            printUser(bootstrap.getUserCurrent());
            printProjects(bootstrap.getProjectService().getProjectsByUserId(bootstrap.getUserCurrent().getId()));
            System.out.println();
            System.out.println("tasks:");
            printTasks(bootstrap.getTaskService().getTasksByUserId(bootstrap.getUserCurrent().getId()));
            System.out.println("[OK]");
        }
    }
}
