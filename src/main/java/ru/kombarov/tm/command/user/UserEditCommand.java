package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UserEditCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "user-edit";
    }

    @Override
    public String description() {
        return "Edit user.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("[USER EDIT]");
            System.out.println("YOUR USERNAME IS " + bootstrap.getUserCurrent().getLogin());
            System.out.println("ENTER NEW USERNAME");
            String login = input.readLine();
            User user = new User();
            user = bootstrap.getUserCurrent();
            user.setLogin(login);
            bootstrap.setUserCurrent(user);
            bootstrap.getUserService().merge(user);
            System.out.println("[OK]");
        }
    }
}
