package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;

public class UserLogoutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user-logout";
    }

    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent() == null) System.out.println("AUTHORIZATION REQUIRED");
        else {
            System.out.println("USER LOG OUT");
            bootstrap.setUserCurrent(null);
            System.out.println("[OK]");
        }
    }
}
