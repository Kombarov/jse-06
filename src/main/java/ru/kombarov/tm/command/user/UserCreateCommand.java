package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.enumerated.Role;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.PasswordUtil.generateHash;

public class UserCreateCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "user-create";
    }

    @Override
    public String description() {
        return "Create new User.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER USERNAME");
        String name = input.readLine();
        for (User user : bootstrap.getUserService().findAll()) {
            while (user.getLogin().equals(name)) {
                System.out.println("THIS LOGIN EXISTS");
                System.out.println("ENTER ANOTHER NAME");
                name = input.readLine();
            }
        }
        System.out.println("TYPE YOUR ROLE: USER OR ADMINISTRATOR");
        String role = input.readLine();
        User user = new User(Role.valueOf(role.toUpperCase()));
        user.setLogin(name);
        System.out.println("ENTER PASSWORD");
        String hash = generateHash(input.readLine());
        user.setPassword(hash);
        bootstrap.getUserService().persist(user);
        System.out.println("[OK]");
    }
}
