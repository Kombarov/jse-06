package ru.kombarov.tm.command.user;

import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.enumerated.Role;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static ru.kombarov.tm.util.EntityUtil.printUsers;

public class UserRemoveCommand extends AbstractCommand {

    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String command() {
        return "user-remove";
    }

    @Override
    public String description() {
        return "Remove existing user";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getUserCurrent().getRole().equals(Role.ADMINISTRATOR)) {
            System.out.println("[USER REMOVE]");
            printUsers(bootstrap.getUserService().findAll());
            System.out.println("TYPE USER NAME TO REMOVE");
            String name = input.readLine();
            bootstrap.getUserService().remove(bootstrap.getUserService().getUserIdByName(name));
            System.out.println("[OK]");
        }
        else System.out.println("ACCESS IS DENIED");
    }
}
