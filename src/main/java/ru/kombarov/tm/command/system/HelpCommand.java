package ru.kombarov.tm.command.system;

import ru.kombarov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() throws Exception {
        for (final AbstractCommand command : bootstrap.getCommands()) {
            System.out.println(command.command() + ": " + command.description());
        }
    }
}
